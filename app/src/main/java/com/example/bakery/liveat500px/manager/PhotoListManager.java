package com.example.bakery.liveat500px.manager;

import android.content.Context;

import com.example.bakery.liveat500px.dao.PhotoItemCollectionDao;
import com.example.bakery.liveat500px.dao.PhotoItemDao;

import java.util.ArrayList;


/**
 * Created by nuuneoi on 11/16/2014.
 */
public class PhotoListManager {

    private Context mContext;

    private PhotoItemCollectionDao dao;

    public PhotoListManager() {
        mContext = ContextManager.getInstance().getContext();
    }

    public PhotoItemCollectionDao getDao() {
        return dao;
    }

    public void setDao(PhotoItemCollectionDao dao) {
        this.dao = dao;
    }

    public void insertDaoAtTopPosition(PhotoItemCollectionDao newDao) {
        if (dao == null) {
            dao = new PhotoItemCollectionDao();

        } else if (dao.getData() == null) {
            dao.setData(new ArrayList<PhotoItemDao>());
        }
        dao.getData().addAll(dao.getData().size(), newDao.getData());
    }

    public void appendDaoBottomPosition(PhotoItemCollectionDao newDao) {
        if (dao == null) {
            dao = new PhotoItemCollectionDao();

        } else if (dao.getData() == null) {
            dao.setData(new ArrayList<PhotoItemDao>());
        }
        dao.getData().addAll(0, newDao.getData());
    }

    public int getMaximumId() {
        if (dao == null) {
            return 0;
        } else if (dao.getData() == null) {
            return 0;
        } else if (dao.getData().get(0) == null) {
            return 0;
        }
        int maxId = dao.getData().get(0).getId();

        for (int i = 1; i < dao.getData().size(); i++) {
            maxId = Math.max(maxId, dao.getData().get(i).getId());
        }
        return maxId;
    }

    public int getMinimumId() {
        if (dao == null) {
            return 0;
        } else if (dao.getData() == null) {
            return 0;
        } else if (dao.getData().get(0) == null) {
            return 0;
        }
        int minId = dao.getData().get(0).getId();

        for (int i = 1; i < dao.getData().size(); i++) {
            minId = Math.min(minId, dao.getData().get(i).getId());
        }
        return minId;
    }

    public int getCont() {
        if (dao == null) {
            return 0;
        } else if (dao.getData() == null) {
            return 0;
        }
        return dao.getData().size();
    }
}
