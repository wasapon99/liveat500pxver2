package com.example.bakery.liveat500px.fragment.main.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.bakery.liveat500px.R;
import com.example.bakery.liveat500px.manager.ContextManager;

public class MainViewHolder extends RecyclerView.ViewHolder {
    ImageView ivImage;
    TextView tvTitle;
    TextView tvSubTitle;
    View vClick;

    MainViewHolder(View itemView) {
        super(itemView);
        ivImage = itemView.findViewById(R.id.ivImage);
        tvTitle = itemView.findViewById(R.id.tvTitle);
        tvSubTitle = itemView.findViewById(R.id.tvSubtitle);
        vClick = itemView.findViewById(R.id.vClick);
    }

    public void setTvTitle(String title) {
        tvTitle.setText(title);
    }

    public void setTvSubTitle(String subTitle) {
        tvSubTitle.setText(subTitle);
    }

    public void setIvImage(String url) {
        Glide.with(ContextManager.getInstance().getContext())
                .load(url)
                .apply(new RequestOptions().
                        placeholder(R.drawable.loading).
                        error(R.drawable.ic_launcher_foreground))
                .into(ivImage);
    }

    public void click(final int position, final MainClickListener listener) {
        vClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onClick(position);
                }
            }
        });
    }
}
