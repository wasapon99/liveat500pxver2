package com.example.bakery.liveat500px.fragment.main.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import com.example.bakery.liveat500px.R;
import com.example.bakery.liveat500px.dao.PhotoItemCollectionDao;
import com.example.bakery.liveat500px.dao.PhotoItemDao;
import com.example.bakery.liveat500px.manager.ContextManager;

public class MainAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>  {
    private PhotoItemCollectionDao dao;
    private int lastPosition = -1;
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    private MainClickListener listener;

    public MainAdapter(MainClickListener listener) {
        this.listener = listener;
    }

    public void setDao(PhotoItemCollectionDao dao) {
        this.dao = dao;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view;
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_photo, parent, false);
            return new MainViewHolder(view);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view;
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_progressbar, parent, false);
            return new LoadingViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof MainViewHolder) {
            PhotoItemDao dao = this.dao.getData().get(position);
            ((MainViewHolder) holder).setTvTitle(dao.getCaption());
            ((MainViewHolder) holder).setTvSubTitle(dao.getUsername() + "\n" + dao.getCamera());
            ((MainViewHolder) holder).setIvImage(dao.getImageUrl());
            ((MainViewHolder) holder).click(position,listener);

            setAnimation(holder.itemView, position);
        } else if (holder instanceof LoadingViewHolder) {
            ((LoadingViewHolder) holder).pbProgress.setIndeterminate(true);
        }


    }

    private void setAnimation(View itemView, int position) {
        if (position > lastPosition) {
            Animation anim = AnimationUtils.loadAnimation(ContextManager.getInstance().getContext(),
                    R.anim.up_from_bottom);
            itemView.startAnimation(anim);
            lastPosition = position;
        }

    }

    @Override
    public int getItemCount() {
        if (dao == null)
            return 0;
        if (dao.getData() == null)
            return 0;
        return dao.getData().size();
    }

    @Override
    public int getItemViewType(int position) {
        return position == this.dao.getData().size() - 1 ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }


}
