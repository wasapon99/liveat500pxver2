package com.example.bakery.liveat500px.fragment.main.adapter;

public interface MainClickListener {
    void onClick(int position);
}
