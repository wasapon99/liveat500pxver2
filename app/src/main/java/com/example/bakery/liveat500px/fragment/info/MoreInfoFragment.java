package com.example.bakery.liveat500px.fragment.info;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.bakery.liveat500px.R;
import com.example.bakery.liveat500px.dao.PhotoItemDao;
import com.example.bakery.liveat500px.manager.ContextManager;

public class MoreInfoFragment extends Fragment {
    private ImageView ivImage;
    private TextView tvTitle;
    private TextView tvSubTitle;

    PhotoItemDao dao;

    public MoreInfoFragment() {
        super();
    }

    public static MoreInfoFragment newInstance(PhotoItemDao dao) {
        MoreInfoFragment fragment = new MoreInfoFragment();
        Bundle args = new Bundle();
        args.putParcelable("dao", dao);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init(savedInstanceState);

        dao = getArguments().getParcelable("dao");

        if (savedInstanceState != null)
            onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_morinfo, container, false);
        initInstances(rootView, savedInstanceState);
        return rootView;
    }

    @SuppressWarnings("UnusedParameters")
    private void init(Bundle savedInstanceState) {
        // Init Fragment level's variable(s) here
    }

    @SuppressWarnings("UnusedParameters")
    private void initInstances(View rootView, Bundle savedInstanceState) {
        ivImage = rootView.findViewById(R.id.ivImage);
        tvTitle = rootView.findViewById(R.id.tvTitle);
        tvSubTitle = rootView.findViewById(R.id.tvSubtitle);
        setView();

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance (Fragment level's variables) State here
    }

    @SuppressWarnings("UnusedParameters")
    private void onRestoreInstanceState(Bundle savedInstanceState) {
        // Restore Instance (Fragment level's variables) State here
    }

    public void setView() {
        tvTitle.setText(dao.getCaption());
        tvSubTitle.setText(dao.getUsername() + "\n" + dao.getCamera());

        Glide.with(ContextManager.getInstance().getContext())
                .load(dao.getImageUrl())
                .apply(new RequestOptions().
                        placeholder(R.drawable.loading).
                        error(R.drawable.ic_launcher_foreground))
                .into(ivImage);
    }

}
