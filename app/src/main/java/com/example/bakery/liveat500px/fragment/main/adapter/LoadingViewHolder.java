package com.example.bakery.liveat500px.fragment.main.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.example.bakery.liveat500px.R;

public class LoadingViewHolder extends RecyclerView.ViewHolder {
    public ProgressBar pbProgress;

    public LoadingViewHolder(View itemView) {
        super(itemView);
        pbProgress = itemView.findViewById(R.id.pbProgress);
    }
}
