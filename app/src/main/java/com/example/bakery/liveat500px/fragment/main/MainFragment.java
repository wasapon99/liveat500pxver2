package com.example.bakery.liveat500px.fragment.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.bakery.liveat500px.R;
import com.example.bakery.liveat500px.activity.MoreInfoActivity;
import com.example.bakery.liveat500px.dao.PhotoItemCollectionDao;
import com.example.bakery.liveat500px.dao.PhotoItemDao;
import com.example.bakery.liveat500px.fragment.main.adapter.MainAdapter;
import com.example.bakery.liveat500px.fragment.main.adapter.MainClickListener;
import com.example.bakery.liveat500px.manager.ContextManager;
import com.example.bakery.liveat500px.manager.HttpManager;
import com.example.bakery.liveat500px.manager.PhotoListManager;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by nuuneoi on 11/16/2014.
 */
public class MainFragment extends Fragment implements MainClickListener {
    private RecyclerView rvRecycle;
    private MainAdapter adapter;
    private SwipeRefreshLayout slSwipe;

    PhotoListManager photoListManager;
    int pastVisibleItems, visibleItemCount, totalItemCount;
    boolean isLoadingMore = false;


    public interface FragmentListener {
        void onPhotoItemClicked(PhotoItemDao dao);
    }

    public static MainFragment newInstance() {
        MainFragment fragment = new MainFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init(savedInstanceState);

        if (savedInstanceState != null)
            onRestoreInstanceState(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        initInstances(rootView, savedInstanceState);
        createAdapter();
        return rootView;
    }

    private void createAdapter() {
        final LinearLayoutManager layoutManager;
        layoutManager = new LinearLayoutManager(getContext());
        rvRecycle.setLayoutManager(layoutManager);

        adapter = new MainAdapter(this);
        rvRecycle.setAdapter(adapter);

        rvRecycle.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                slSwipe.setEnabled(dx == 0);
                visibleItemCount = layoutManager.getChildCount();
                totalItemCount = layoutManager.getItemCount();
                pastVisibleItems = layoutManager.findFirstCompletelyVisibleItemPosition();
                if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                    loadMoreData();
                }
            }
        });
    }

    @SuppressWarnings("UnusedParameters")
    private void init(Bundle savedInstanceState) {
        // Init Fragment level's variable(s) here
    }

    @SuppressWarnings("UnusedParameters")
    private void initInstances(View rootView, Bundle savedInstanceState) {
        photoListManager = new PhotoListManager();
        rvRecycle = rootView.findViewById(R.id.rvRecycle);
        slSwipe = rootView.findViewById(R.id.slSwipe);

        slSwipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshData();
            }
        });


        refreshData();
    }

    private void refreshData() {
        if (photoListManager.getCont() == 0) {
            reloadData();

        } else {
            reloadDataNewer();
        }
    }

    private void reloadDataNewer() {
        int maxId = photoListManager.getMaximumId();
        Call<PhotoItemCollectionDao> call = HttpManager.getInstance().getService().loadPhotoListAfterId(maxId);
        call.enqueue(new PhotoListLoadCallback(PhotoListLoadCallback.MODE_RELOAD_NEWER));
    }

    public void loadMoreData() {
        if (isLoadingMore)
            return;
        isLoadingMore = true;
        int minId = photoListManager.getMinimumId();
        Call<PhotoItemCollectionDao> call = HttpManager.getInstance().getService().loadPhotoListBeforeId(minId);
        call.enqueue(new PhotoListLoadCallback(PhotoListLoadCallback.MODE_LOAD_MORE));

    }

    private void reloadData() {
        Call<PhotoItemCollectionDao> call = HttpManager.getInstance().getService().loadPhotoList();
        call.enqueue(new PhotoListLoadCallback(PhotoListLoadCallback.MODE_RELOAD));
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance (Fragment level's variables) State here
    }

    @SuppressWarnings("UnusedParameters")
    private void onRestoreInstanceState(Bundle savedInstanceState) {
        // Restore Instance (Fragment level's variables) State here
    }

    @Override
    public void onClick(int position) {
        if (position < photoListManager.getCont()) {
            PhotoItemDao dao = photoListManager.getDao().getData().get(position);
            FragmentListener listener = (FragmentListener) getActivity();
            if (listener != null) {
                listener.onPhotoItemClicked(dao);
            }
        }
    }

    class PhotoListLoadCallback implements Callback<PhotoItemCollectionDao> {
        public static final int MODE_RELOAD = 1;
        public static final int MODE_RELOAD_NEWER = 2;
        public static final int MODE_LOAD_MORE = 3;

        int mode;

        public PhotoListLoadCallback(int mode) {
            this.mode = mode;
        }

        @Override
        public void onResponse(Call<PhotoItemCollectionDao> call, Response<PhotoItemCollectionDao> response) {
            slSwipe.setRefreshing(false);
            if (response.isSuccessful()) {
                PhotoItemCollectionDao dao = response.body();
                if (mode == MODE_RELOAD_NEWER) {
                    photoListManager.insertDaoAtTopPosition(dao);
                } else if (mode == MODE_LOAD_MORE) {
                    photoListManager.appendDaoBottomPosition(dao);
                    isLoadingMore = false;
                } else {
                    photoListManager.setDao(dao);
                }
                photoListManager.insertDaoAtTopPosition(dao);
                adapter.setDao(photoListManager.getDao());
                adapter.notifyDataSetChanged();
                Toast.makeText(ContextManager.getInstance().getContext(), "Load Complete", Toast.LENGTH_SHORT).show();
            } else {
                if (mode == MODE_LOAD_MORE) {
                    isLoadingMore = false;
                }
                try {
                    Toast.makeText(ContextManager.getInstance().getContext(), response.errorBody().string(), Toast.LENGTH_SHORT).show();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<PhotoItemCollectionDao> call, Throwable t) {
            if (mode == MODE_LOAD_MORE) {
                isLoadingMore = false;
            }
            slSwipe.setRefreshing(false);
            Toast.makeText(ContextManager.getInstance().getContext(), t.toString(), Toast.LENGTH_SHORT).show();
        }
    }

}
