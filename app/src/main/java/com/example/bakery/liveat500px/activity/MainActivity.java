package com.example.bakery.liveat500px.activity;

import android.content.Intent;
import android.content.res.Configuration;
import android.support.annotation.Nullable;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.example.bakery.liveat500px.R;
import com.example.bakery.liveat500px.dao.PhotoItemDao;
import com.example.bakery.liveat500px.fragment.main.MainFragment;

public class MainActivity extends AppCompatActivity implements MainFragment.FragmentListener {
    DrawerLayout dlDrawerLayout;
    ActionBarDrawerToggle actionBarDrawerToggle;
    Toolbar tlToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initInstance();

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.flContainer, MainFragment.newInstance())
                    .commit();
        }
    }

    private void initInstance() {
        tlToolbar = findViewById(R.id.tlToolbar);
        setSupportActionBar(tlToolbar);

        dlDrawerLayout = findViewById(R.id.dlDrawerLayout);
        actionBarDrawerToggle = new ActionBarDrawerToggle(
                this, dlDrawerLayout, R.string.open_drawer, R.string.close_drawe
        );
        dlDrawerLayout.addDrawerListener(actionBarDrawerToggle);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        actionBarDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        actionBarDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(actionBarDrawerToggle.onOptionsItemSelected(item))
            return true;
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPhotoItemClicked(PhotoItemDao dao) {
        Intent intent = new Intent(this, MoreInfoActivity.class);
        intent.putExtra("dao", dao);
        startActivity(intent);
    }
}
