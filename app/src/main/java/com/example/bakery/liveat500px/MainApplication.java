package com.example.bakery.liveat500px;

import android.app.Application;

import com.example.bakery.liveat500px.manager.ContextManager;

public class MainApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        ContextManager.getInstance().init(getApplicationContext());
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }
}
